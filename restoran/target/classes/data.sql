INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');

INSERT INTO kategorija (`id`, `naziv_kategorije`) VALUES ('1', 'pizza');
INSERT INTO kategorija (`id`, `naziv_kategorije`) VALUES ('2', 'pasta');
INSERT INTO kategorija (`id`, `naziv_kategorije`) VALUES ('3', 'kafa');
INSERT INTO kategorija (`id`, `naziv_kategorije`) VALUES ('4', 'vino');
INSERT INTO kategorija (`id`, `naziv_kategorije`) VALUES ('5', 'sok');

INSERT INTO jelovnik (`id`, `cena`, `naziv`, `kategorija_id`) VALUES ('1', '760', 'Pizza Romana', '1');
INSERT INTO jelovnik (`id`, `cena`, `naziv`, `kategorija_id`) VALUES ('2', '560', 'Canzona', '1');
INSERT INTO jelovnik (`id`, `cena`, `naziv`, `kategorija_id`) VALUES ('3', '600', 'Bolognese', '2');
INSERT INTO jelovnik (`id`, `cena`, `naziv`, `kategorija_id`) VALUES ('4', '190', 'Espresso', '3');
INSERT INTO jelovnik (`id`, `cena`, `naziv`, `kategorija_id`) VALUES ('5', '200', 'Coca-Cola', '5');
INSERT INTO jelovnik (`id`, `cena`, `naziv`, `kategorija_id`) VALUES ('6', '1700', 'Kovacevic', '4'); 
INSERT INTO jelovnik (`id`, `cena`, `naziv`, `kategorija_id`) VALUES ('7', '190', 'Nes', '3');
INSERT INTO jelovnik (`id`, `cena`, `naziv`, `kategorija_id`) VALUES ('8', '200', 'Sprite', '5');
INSERT INTO jelovnik (`id`, `cena`, `naziv`, `kategorija_id`) VALUES ('9', '15', 'Vino iz TETRAPAKA', '4'); 





