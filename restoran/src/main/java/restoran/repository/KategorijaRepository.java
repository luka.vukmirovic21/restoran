package restoran.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import restoran.model.Kategorija;

@Repository
public interface KategorijaRepository extends JpaRepository<Kategorija, Long>{

	Kategorija findOneById(Long id);

	Kategorija findOneByNazivKategorije(String kategorijaNaziv);

}
