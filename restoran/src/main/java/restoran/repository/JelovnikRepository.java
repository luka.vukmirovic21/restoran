package restoran.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import restoran.model.Jelovnik;

@Repository
public interface JelovnikRepository extends JpaRepository<Jelovnik, Long> {

	Jelovnik findOneById(Long id);

	Page<Jelovnik> findByNazivIgnoreCaseContainsOrderByCenaDesc(String naziv, Pageable pageable);
	
	Page<Jelovnik> findByNazivIgnoreCaseContainsOrderByCenaAsc(String naziv, Pageable pageable);

	Page<Jelovnik> findByNazivIgnoreCaseContainsOrderByKategorijaNazivKategorijeAsc(String naziv, Pageable pageable);

	Page<Jelovnik> findByNazivIgnoreCaseContainsOrderByKategorijaNazivKategorijeDesc(String naziv, Pageable pageable);

	Page<Jelovnik> findByNazivIgnoreCaseContainsOrderByIdAsc(String naziv, Pageable pageable);



}
