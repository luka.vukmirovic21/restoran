package restoran.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import restoran.model.Kategorija;
import restoran.web.dto.KategorijaDTO;

@Component
public class KategorijaToKategorijaDTO implements Converter<Kategorija, KategorijaDTO>{

	@Override
	public KategorijaDTO convert(Kategorija source) {
		KategorijaDTO dto = new KategorijaDTO();
		
		dto.setId(source.getId());
		dto.setNazivKategorije(source.getNazivKategorije());
	
		return dto;
	}
	
	public List<KategorijaDTO> convert(List<Kategorija> kategorije) {
		List<KategorijaDTO> dtos = new ArrayList<>();

		for (Kategorija i : kategorije) {
			KategorijaDTO dto = convert(i);
			dtos.add(dto);
		}

		return dtos;
	}

}
