package restoran.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import restoran.model.Jelovnik;
import restoran.web.dto.JelovnikDTO;

@Component
public class JelovnikToJelovnikDTO implements Converter<Jelovnik, JelovnikDTO>{

	@Override
	public JelovnikDTO convert(Jelovnik source) {
		JelovnikDTO dto = new JelovnikDTO();
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setCena(source.getCena());
		dto.setKategorijaId(source.getKategorija().getId());
		dto.setKategorijaNaziv(source.getKategorija().getNazivKategorije());
		
		return dto;
	}	
	
	public List<JelovnikDTO> convert(List<Jelovnik> js) {
		List<JelovnikDTO> dtos = new ArrayList<>();

		for (Jelovnik j : js) {
			JelovnikDTO dto = convert(j);
			dtos.add(dto);
		}

		return dtos;
	}

}
