package restoran.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import restoran.model.Jelovnik;
import restoran.service.JelovnikService;
import restoran.service.KategorijaService;
import restoran.web.dto.JelovnikDTO;

@Component
public class JelovnikDtoToJelovnik implements Converter<JelovnikDTO, Jelovnik>{

	@Autowired
	private JelovnikService jelovnikService;
	@Autowired
	private KategorijaService kategorijaService;

	@Override
	public Jelovnik convert(JelovnikDTO dto) {
		Jelovnik entity;

        if(dto.getId() == null) {
            entity = new Jelovnik();
        }else {
            entity = jelovnikService.findOneById(dto.getId());
        }

        if(entity != null) {
        	
        	entity.setNaziv(dto.getNaziv());
        	entity.setCena(dto.getCena());
        	entity.setKategorija(kategorijaService.findOneByNazivKategorije(dto.getKategorijaNaziv()));
             
        }

        return entity;
    }
 
}
