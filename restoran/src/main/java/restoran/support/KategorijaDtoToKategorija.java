package restoran.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import restoran.model.Kategorija;
import restoran.service.KategorijaService;
import restoran.web.dto.KategorijaDTO;

@Component
public class KategorijaDtoToKategorija implements Converter<KategorijaDTO, Kategorija>{
	
	@Autowired
	private KategorijaService kategorijaService;

	@Override
	public Kategorija convert(KategorijaDTO dto) {
		Kategorija entity;

        if(dto.getId() == null) {
            entity = new Kategorija();
        }else {
            entity = kategorijaService.findOneById(dto.getId());
        }

        if(entity != null) {
        	
        	entity.setNazivKategorije(dto.getNazivKategorije());
            
        }

        return entity;
    }

}
