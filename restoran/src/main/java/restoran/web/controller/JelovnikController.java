package restoran.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import restoran.model.Jelovnik;
import restoran.service.JelovnikService;
import restoran.support.JelovnikDtoToJelovnik;
import restoran.support.JelovnikToJelovnikDTO;
import restoran.web.dto.JelovnikDTO;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/jelovnici", produces = MediaType.APPLICATION_JSON_VALUE)
public class JelovnikController {
	@Autowired
	private JelovnikService jelovnikService;
	@Autowired
	private JelovnikToJelovnikDTO toJelovnikDto;
	@Autowired
	private JelovnikDtoToJelovnik toJelovnik;
	
	@GetMapping
	public ResponseEntity<List<JelovnikDTO>> get(@RequestParam(defaultValue = "0") int page,
												 @RequestParam(defaultValue = "2") int size,
			   									 @RequestParam(required= false, defaultValue = "") String sortBy,
		   									 	 @RequestParam(required=false,  defaultValue= "") String naziv){
		Page<Jelovnik> js = jelovnikService.findAllSorted(page, sortBy, naziv, size);
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("TotalPages", Integer.toString(js.getTotalPages()));
		return new ResponseEntity<>(toJelovnikDto.convert(js.getContent()),headers, HttpStatus.OK);
	}	
	
	@GetMapping("/{id}")
	public ResponseEntity<JelovnikDTO> get(@PathVariable Long id) {
		Optional<Jelovnik> jelovnik = jelovnikService.findOne(id);

		if (jelovnik.isPresent()) {
			return new ResponseEntity<>(toJelovnikDto.convert(jelovnik.get()), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<JelovnikDTO> create(@Valid @RequestBody JelovnikDTO dto) {
		Jelovnik j = toJelovnik.convert(dto);
		Jelovnik saved = jelovnikService.save(j); 

		return new ResponseEntity<>(toJelovnikDto.convert(saved), HttpStatus.CREATED);
	}
	
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<JelovnikDTO> update(@PathVariable Long id, @Valid @RequestBody JelovnikDTO dto) {

		if (!id.equals(dto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} 
		Jelovnik j = toJelovnik.convert(dto);
		Jelovnik updated = jelovnikService.update(j);

		return new ResponseEntity<>(toJelovnikDto.convert(updated), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Jelovnik deleted = jelovnikService.delete(id);

		if (deleted != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);  
		}
	}

}
