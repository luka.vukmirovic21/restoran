package restoran.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import restoran.model.Jelovnik;
import restoran.model.Kategorija;
import restoran.service.KategorijaService;
import restoran.support.KategorijaDtoToKategorija;
import restoran.support.KategorijaToKategorijaDTO;
import restoran.web.dto.JelovnikDTO;
import restoran.web.dto.KategorijaDTO;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/kategorije", produces = MediaType.APPLICATION_JSON_VALUE)
public class KategorijaController {
	@Autowired
	private KategorijaService kategorijaService;
	@Autowired
	private KategorijaToKategorijaDTO toKategorijaDto;
	@Autowired
	private KategorijaDtoToKategorija toKategorija;
	
	
	@GetMapping("/select")
	    public ResponseEntity<List<KategorijaDTO>> getAll(){

	        List<Kategorija> jelovnici = kategorijaService.findAll();

	        return new ResponseEntity<>(toKategorijaDto.convert(jelovnici), HttpStatus.OK);
	    }	
	
	
	
	@GetMapping
	public ResponseEntity<List<KategorijaDTO>> get(@RequestParam(name ="page", defaultValue = "0") int page,
												   @RequestParam(name ="size", defaultValue = "3") int size) {
		Page<Kategorija> kategorije = kategorijaService.findAll(page, size);
		return new ResponseEntity<>(toKategorijaDto.convert(kategorije.getContent()), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<KategorijaDTO> get(@PathVariable Long id) {
		Optional<Kategorija> ispit = kategorijaService.findOne(id);

		if (ispit.isPresent()) {
			return new ResponseEntity<>(toKategorijaDto.convert(ispit.get()), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
		}
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<KategorijaDTO> create(@Valid @RequestBody KategorijaDTO dto) {
		Kategorija k = toKategorija.convert(dto);
		Kategorija saved = kategorijaService.save(k);

		return new ResponseEntity<>(toKategorijaDto.convert(saved), HttpStatus.CREATED);
	}

}
