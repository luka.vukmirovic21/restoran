package restoran.web.dto;

public class KategorijaDTO {
	
	private Long id;
	private String nazivKategorije;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNazivKategorije() {
		return nazivKategorije;
	}
	public void setNazivKategorije(String nazivKategorije) {
		this.nazivKategorije = nazivKategorije;
	}
	
	
	

}
