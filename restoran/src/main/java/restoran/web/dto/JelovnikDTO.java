package restoran.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class JelovnikDTO {
	
	private Long id;
	@NotBlank
	private String naziv;
	@NotNull
	private Double cena;
	
	private Long kategorijaId;
	@NotBlank
	private String kategorijaNaziv;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public Double getCena() {
		return cena;
	}
	public void setCena(Double cena) {
		this.cena = cena;
	}
	public Long getKategorijaId() {
		return kategorijaId;
	}
	public void setKategorijaId(Long kategorijaId) {
		this.kategorijaId = kategorijaId;
	}
	public String getKategorijaNaziv() {
		return kategorijaNaziv;
	}
	public void setKategorijaNaziv(String kategorijaNaziv) {
		this.kategorijaNaziv = kategorijaNaziv;
	}
	
	
	
	
	
	

}
