package restoran.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Jelovnik {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column
	private String naziv;
	@Column
	private double cena;
	@ManyToOne
	private Kategorija kategorija;
	
	
	
	
	public Jelovnik() {
		
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getNaziv() { 
		return naziv;
	}



	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}



	public double getCena() {
		return cena;
	}



	public void setCena(double cena) {
		this.cena = cena;
	}



	public Kategorija getKategorija() {
		return kategorija;
	}



	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}


	
	
	
	
	
	

}
