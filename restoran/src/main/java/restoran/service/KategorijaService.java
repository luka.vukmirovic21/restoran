package restoran.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import restoran.model.Kategorija;

public interface KategorijaService {

	Page<Kategorija> findAll(int page, int size);

	Optional<Kategorija> findOne(Long id);

	Kategorija findOneById(Long id);

	Kategorija save(Kategorija k);

	Kategorija findOneByNazivKategorije(String kategorijaNaziv);

	List<Kategorija> findAll();

}
