package restoran.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import restoran.model.Jelovnik;

public interface JelovnikService {

	Page<Jelovnik> findAll(int page);

	Optional<Jelovnik> findOne(Long id);

	Jelovnik findOneById(Long id);

	Jelovnik save(Jelovnik j);

	Page<Jelovnik> findAllSorted(int page, String sortBy, String naziv, int size);

	Jelovnik update(Jelovnik j);

	Jelovnik delete(Long id);

}
