package restoran.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import restoran.model.Jelovnik;
import restoran.repository.JelovnikRepository;
import restoran.service.JelovnikService;

@Service
public class JpaJelovnikService implements JelovnikService {
	@Autowired
	private JelovnikRepository jelovnikRepository;

	@Override
	public Page<Jelovnik> findAll(int page) {
		return jelovnikRepository.findAll(PageRequest.of(page, 3));
	}

	@Override
	public Optional<Jelovnik> findOne(Long id) {
		return jelovnikRepository.findById(id);
	}

	@Override
	public Jelovnik findOneById(Long id) {
		return jelovnikRepository.findOneById(id);
	}

	@Override
	public Jelovnik save(Jelovnik j) {
		return jelovnikRepository.save(j);
	}

	@Override
	public Page<Jelovnik> findAllSorted(int page, String sortBy, String naziv, int size) {
		if(sortBy.contains("priceDesc")) {
			return jelovnikRepository.findByNazivIgnoreCaseContainsOrderByCenaDesc(naziv, PageRequest.of(page, 4));
		} else if (sortBy.contains("priceAsc")) {
			return jelovnikRepository.findByNazivIgnoreCaseContainsOrderByCenaAsc(naziv,PageRequest.of(page, 4));
		} else if(sortBy.contains("categoryAsc")) {
			return jelovnikRepository.findByNazivIgnoreCaseContainsOrderByKategorijaNazivKategorijeAsc(naziv, PageRequest.of(page, 4));
		} else if (sortBy.contains("categoryDesc")) {
			return jelovnikRepository.findByNazivIgnoreCaseContainsOrderByKategorijaNazivKategorijeDesc(naziv, PageRequest.of(page, 4));
		}
	   return jelovnikRepository.findByNazivIgnoreCaseContainsOrderByIdAsc(naziv ,PageRequest.of(page, 4));
	}

	@Override
	public Jelovnik update(Jelovnik j) {
		return jelovnikRepository.save(j);
	}

	 @Override
	    public Jelovnik delete(Long id) {
		 Jelovnik jelovnik = findOneById(id);
	        if(jelovnik != null){
	        	jelovnik.getKategorija().getJelovnici().remove(jelovnik);
	        	jelovnik.setKategorija(null);
	        	jelovnik = jelovnikRepository.save(jelovnik);
	        	jelovnikRepository.delete(jelovnik);
	            return jelovnik;
	        }
	        return null;
	    }


}
