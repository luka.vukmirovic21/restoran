package restoran.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import restoran.model.Kategorija;
import restoran.repository.KategorijaRepository;
import restoran.service.KategorijaService;

@Service
public class JpaKategorijaService implements KategorijaService{
	@Autowired
	private KategorijaRepository kategorijaRepository;

	@Override
	public Page<Kategorija> findAll(int page, int size) {
		return kategorijaRepository.findAll(PageRequest.of(page, size));
	}

	@Override
	public Optional<Kategorija> findOne(Long id) {
		return kategorijaRepository.findById(id);
	}

	@Override
	public Kategorija findOneById(Long id) {
		return kategorijaRepository.findOneById(id);
	}

	@Override
	public Kategorija save(Kategorija k) {
		return kategorijaRepository.save(k);
	}

	@Override
	public Kategorija findOneByNazivKategorije(String kategorijaNaziv) {
		return kategorijaRepository.findOneByNazivKategorije(kategorijaNaziv);
	}

	@Override
	public List<Kategorija> findAll() {
		return kategorijaRepository.findAll();
	}

}
