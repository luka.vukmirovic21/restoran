import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Kategorija, KategorijaInterface} from "../../model/kategorija.model";
import {KategorijaService} from "../../service/kategorija.service";
import {HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-kategorije-page',
  templateUrl: './kategorije-page.component.html',
  styleUrls: ['./kategorije-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class KategorijePageComponent implements OnInit {

  public kategorije: Kategorija[];




  constructor(
    private kategorijaService: KategorijaService
  ) { }

  ngOnInit() {
    this.loadData()
  }


  loadData() {
    this.kategorijaService.getKategorije().subscribe(
      kategorije => this.kategorije = kategorije
    );
  }

  addKategorija(kategorija: Kategorija) {
this.kategorijaService.postKategorija(kategorija).subscribe(res =>this.loadData())
  }


}
