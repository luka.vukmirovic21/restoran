import { Component, OnInit } from '@angular/core';
import {Jelovnik} from "../../model/jelovnik.model";
import {ActivatedRoute} from "@angular/router";
import {JelovnikService} from "../../service/jelovnik.service";

@Component({
  selector: 'app-jelovnik-details-page',
  templateUrl: './jelovnik-details-page.component.html',
  styleUrls: ['./jelovnik-details-page.component.css']
})
export class JelovnikDetailsPageComponent implements OnInit {

   id: number;
   jelovnik: Jelovnik;

  constructor(private route: ActivatedRoute, private jelovnikServie: JelovnikService) { }

  ngOnInit() {
    this.loadData()
  }

  loadData() {
    this.route.params.subscribe(param => {
      this.id = param.id;
      this.jelovnikServie.getJelovnik(this.id)
        .subscribe((jelovnik: Jelovnik) => this.jelovnik = jelovnik);
    });
  }

}
