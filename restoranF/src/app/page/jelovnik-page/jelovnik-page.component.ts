import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Jelovnik, JelovnikInterface} from "../../model/jelovnik.model";
import {JelovnikService} from "../../service/jelovnik.service";
import {Kategorija} from "../../model/kategorija.model";
import {KategorijaService} from "../../service/kategorija.service";
import {HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-jelovnik-page',
  templateUrl: './jelovnik-page.component.html',
  styleUrls: ['./jelovnik-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class JelovnikPageComponent implements OnInit {

  public jelovnici: Jelovnik[];
  public page = 0;
  public sortBy = "";
  public kategorije: Kategorija[];
  public lp;
  public activeJelovnik: JelovnikInterface;
  public showForm: boolean;


  constructor(
    private jelovnikService: JelovnikService,
    private kategorijaService: KategorijaService
  ) { }

  ngOnInit(){
     this.loadData()

  }

  showConfigResponse() {
    this.jelovnikService.getConfigResponse()
      .subscribe(resp => {
        const keys = resp.headers.get('TotalPages');
        this.lp=keys
      });
  }


  loadData() {
    this.jelovnikService.getJelovnikPaged(this.page, this.sortBy).subscribe(
      jelovnici => this.jelovnici = jelovnici
    );
    this.loadKategorije()
    this.showConfigResponse()


  }

  loadKategorije() {
    this.kategorijaService.getKategorije().subscribe(
      kategorije => this.kategorije = kategorije
    );
  }


  editJelovnik(jelovnik: Jelovnik) {
    this.jelovnikService.putJelovnik(jelovnik).subscribe(res => {
      this.resetActiveJelovnik();
      this.loadData();
    })
  }

  resetActiveJelovnik() {
    this.activeJelovnik = new Jelovnik({
      // id: 0,
      naziv: '',
      cena: 0,
      kategorijaNaziv: '',
      // kategorijaId: 0
    })
  }

  setActiveJelovnik(jelovnik: Jelovnik) {
    this.activeJelovnik = new Jelovnik(jelovnik);
    this.showForm= !this.showForm;
  }


  search(naziv: string,sortBy: string) {
    this.jelovnikService.searchJelovnici(naziv, sortBy).subscribe((jelovnici: Jelovnik[]) => this.jelovnici = jelovnici);
  }
  addJelovnik(jelovnik: Jelovnik) {
    this.jelovnikService.postJelovnik(jelovnik).subscribe(res => this.loadData())
  }

  deleteJelovnik(id: number) {
    this.jelovnikService.deleteJelovnik(id).subscribe(res => this.loadData());
  }

  changePage(page: number) {
    this.page = page;
    this.loadData();

  }


}
