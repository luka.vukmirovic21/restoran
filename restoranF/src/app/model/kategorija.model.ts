export interface KategorijaInterface {
  id?:number;
  nazivKategorije: string;
}
export class Kategorija implements  KategorijaInterface {
  public id?: number;
  public nazivKategorije: string;

  constructor(kategorijaCfg: KategorijaInterface) {
    this.id = kategorijaCfg.id;
    this.nazivKategorije= kategorijaCfg.nazivKategorije;
  }
}


