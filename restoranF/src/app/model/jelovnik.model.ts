export interface JelovnikInterface {
    id?: number;
    naziv: string;
    cena: number;

    kategorijaId?: number;
    kategorijaNaziv: string;
}
export class Jelovnik implements JelovnikInterface {
  public cena: number;
  public id?: number;
  public kategorijaId?: number;
  public kategorijaNaziv: string;
  public naziv: string;

  constructor(jelovnikCfg: JelovnikInterface) {
    this.id = jelovnikCfg.id;
    this.cena = jelovnikCfg.cena;
    this.naziv= jelovnikCfg.naziv;
    this.kategorijaId=jelovnikCfg.kategorijaId;
    this.kategorijaNaziv=jelovnikCfg.kategorijaNaziv;
  }

}
