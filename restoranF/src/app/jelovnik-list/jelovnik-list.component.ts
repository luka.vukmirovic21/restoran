import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {Jelovnik, JelovnikInterface} from "../model/jelovnik.model";

@Component({
  selector: 'app-jelovnik-list',
  templateUrl: './jelovnik-list.component.html',
  styleUrls: ['./jelovnik-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class JelovnikListComponent implements OnInit {

  @Input()
  jelovnici: JelovnikInterface[];

  @Output()
  jelovnikDeleted: EventEmitter<number> = new EventEmitter();

  @Output()
  markJelovnikForEditing: EventEmitter<Jelovnik> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  deleteJelovnik(id: number) {
    this.jelovnikDeleted.next(id);
  }

  editJelovnik(jelovnik: Jelovnik) {
    this.markJelovnikForEditing.next(jelovnik);
  }


}
