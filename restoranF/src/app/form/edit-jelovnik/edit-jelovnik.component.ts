import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Jelovnik} from "../../model/jelovnik.model";
import {Kategorija} from "../../model/kategorija.model";

@Component({
  selector: 'app-edit-jelovnik',
  templateUrl: './edit-jelovnik.component.html',
  styleUrls: ['./edit-jelovnik.component.css']
})
export class EditJelovnikComponent implements OnInit {

  @Output()
  jelovnikEdited: EventEmitter<Jelovnik> = new EventEmitter();

  @Input()
  jelovnikToEdit: Jelovnik;
  @Input()
  kategorije: Kategorija[];



  constructor() { }

  ngOnInit(){
  }

  editJelovnik() {
    this.jelovnikEdited.next(this.jelovnikToEdit);
  }



}
