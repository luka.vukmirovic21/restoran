import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Jelovnik} from "../../model/jelovnik.model";
import {Kategorija} from "../../model/kategorija.model";

@Component({
  selector: 'app-add-jelovnik',
  templateUrl: './add-jelovnik.component.html',
  styleUrls: ['./add-jelovnik.component.css']
})
export class AddJelovnikComponent implements OnInit {

  @Output()
  jelovnikAdded: EventEmitter<Jelovnik> = new EventEmitter();

  jelovnikToAdd: Jelovnik;
  @Input()
  kategorije: Kategorija[];
  showForm: boolean;

  constructor() { }

  ngOnInit() {
    this.resetJelovnikToAdd();
  }

  addJelovnik() {
    this.jelovnikAdded.next(this.jelovnikToAdd);
    this.resetJelovnikToAdd();
  }

  resetJelovnikToAdd() {
    this.jelovnikToAdd = new Jelovnik({
      naziv: '',
      cena: 0,
      kategorijaNaziv: '',
    });
  }



}
