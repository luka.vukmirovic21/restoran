import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Kategorija} from "../../model/kategorija.model";

@Component({
  selector: 'app-add-kategorija',
  templateUrl: './add-kategorija.component.html',
  styleUrls: ['./add-kategorija.component.css']
})
export class AddKategorijaComponent implements OnInit {

  @Output()
  kategorijaAdded: EventEmitter<Kategorija> = new EventEmitter();

  public kategorijaToAdd: Kategorija;

  constructor() { }

  ngOnInit() {
    this.resetKategorijaToAdd();
  }

  addKategorija() {
    this.kategorijaAdded.next(this.kategorijaToAdd);
    this.resetKategorijaToAdd();
  }

  resetKategorijaToAdd() {
    this.kategorijaToAdd = new Kategorija({
        nazivKategorije: ""
    });
  }

}
