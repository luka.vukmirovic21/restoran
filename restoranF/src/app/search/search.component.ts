import {Component, EventEmitter, OnInit, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SearchComponent implements OnInit {

  @Output()
  setSearchTerm = new EventEmitter<{searchTerm: string, searchTerm2: string}>();

  searchTerm: string;
  searchTerm2: string;

  constructor() {
    this.searchTerm = "";
  }

  ngOnInit() {
  }

  search() {
    this.setSearchTerm.next({searchTerm: this.searchTerm, searchTerm2: this.searchTerm2});

  }

  reset() {
    this.searchTerm = '';
    this.searchTerm2= '';
    this.setSearchTerm.next({searchTerm: this.searchTerm, searchTerm2: this.searchTerm2});
  }

}
