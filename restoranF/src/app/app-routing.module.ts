import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {KategorijePageComponent} from "./page/kategorije-page/kategorije-page.component";
import {JelovnikPageComponent} from "./page/jelovnik-page/jelovnik-page.component";
import {JelovnikDetailsPageComponent} from "./page/jelovnik-details-page/jelovnik-details-page.component";
import {PageNotFoundComponent} from "./page/page-not-found/page-not-found.component";

const routes: Routes = [
  { path: 'jelovnici', component: JelovnikPageComponent },
  { path: 'jelovnici/:id', component: JelovnikDetailsPageComponent },
  { path: 'kategorije', component: KategorijePageComponent },
  { path: '', redirectTo: 'jelovnici', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
