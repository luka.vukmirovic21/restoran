import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Kategorija} from "../model/kategorija.model";

@Injectable({
  providedIn: 'root'
})
export class KategorijaService {

  private readonly  path = 'http://localhost:8080/api/kategorije'

  constructor( private http: HttpClient) { }

  getKategorije(): Observable<Kategorija[]> {
    return this.http.get<Kategorija[]>(`${this.path}/select`);
  }


  getKategorijePaged(page, size): Observable<Kategorija[]> {
    const params = new HttpParams()
      .set('page', page)
      .set('size', size);

    return this.http.get<Kategorija[]>(this.path, { params });
  }

  postKategorija(kategorija: Kategorija): Observable<Kategorija> {
    return this.http.post<Kategorija>(this.path, kategorija);
  }
}
