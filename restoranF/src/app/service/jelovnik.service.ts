import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {Jelovnik} from "../model/jelovnik.model";

@Injectable({
  providedIn: 'root'
})
export class JelovnikService {

  private readonly  path = 'http://localhost:8080/api/jelovnici'


  constructor( private http: HttpClient) { }


  getConfigResponse(): Observable<HttpResponse<Jelovnik>> {
    return this.http.get<Jelovnik>(
      this.path, { observe: 'response' });
  }


  getJelovnikPaged(page, sortBy): Observable<Jelovnik[]> {
    const params = new HttpParams()
      .set('page', page)
      .set('sortBy', sortBy);

    return this.http.get<Jelovnik[]>(this.path, { params });
  }

  searchJelovnici(naziv: string, sortBy: string): Observable<Jelovnik[]> {
    const params: HttpParams = new HttpParams().append('naziv', naziv).append('sortBy', sortBy);

    return this.http.get<Jelovnik[]>(this.path, { params });
  }

  postJelovnik(jelovnik: Jelovnik): Observable<Jelovnik> {
    return this.http.post<Jelovnik>(this.path, jelovnik);
  }

  deleteJelovnik(id: number): Observable<Jelovnik> {
    return this.http.delete<Jelovnik>(`${this.path}/${id}`);
  }


  getJelovnik(id: number): Observable<Jelovnik> {
    return this.http.get<Jelovnik>(`${this.path}/${id}`);
  }

  putJelovnik(jelovnik: Jelovnik): Observable<Jelovnik> {
    return this.http.put<Jelovnik>(`${this.path}/${jelovnik.id}`, jelovnik);
  }

}
