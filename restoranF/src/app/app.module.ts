import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KategorijaListComponent } from './kategorija-list/kategorija-list.component';
import { KategorijaListItemComponent } from './kategorija-list-item/kategorija-list-item.component';
import { KategorijePageComponent } from './page/kategorije-page/kategorije-page.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import { JelovnikListComponent } from './jelovnik-list/jelovnik-list.component';
import { JelovnikListItemComponent } from './jelovnik-list-item/jelovnik-list-item.component';
import { JelovnikPageComponent } from './page/jelovnik-page/jelovnik-page.component';
import { SearchComponent } from './search/search.component';
import { AddJelovnikComponent } from './form/add-jelovnik/add-jelovnik.component';
import { JelovnikDetailsPageComponent } from './page/jelovnik-details-page/jelovnik-details-page.component';
import { EditJelovnikComponent } from './form/edit-jelovnik/edit-jelovnik.component';
import { AddKategorijaComponent } from './form/add-kategorija/add-kategorija.component';
import { PageNotFoundComponent } from './page/page-not-found/page-not-found.component';



@NgModule({
  declarations: [
    AppComponent,
    KategorijaListComponent,
    KategorijaListItemComponent,
    KategorijePageComponent,
    JelovnikListComponent,
    JelovnikListItemComponent,
    JelovnikPageComponent,
    SearchComponent,
    AddJelovnikComponent,
    AddJelovnikComponent,
    JelovnikDetailsPageComponent,
    EditJelovnikComponent,
    AddKategorijaComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
