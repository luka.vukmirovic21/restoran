import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {Jelovnik} from "../model/jelovnik.model";

@Component({
  selector: 'tr[app-jelovnik-list-item]',
  templateUrl: './jelovnik-list-item.component.html',
  styleUrls: ['./jelovnik-list-item.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class JelovnikListItemComponent implements OnInit {

  @Input()
  jelovnik: Jelovnik;

  @Output()
  jelovnikDeleted: EventEmitter<number> = new EventEmitter();

  @Output()
  markJelovnikForEditing: EventEmitter<Jelovnik> = new EventEmitter();


  constructor() { }

  ngOnInit(){
  }

  deleteJelovnik() {
    this.jelovnikDeleted.next(this.jelovnik.id);
  }

  editJelovnik() {
    this.markJelovnikForEditing.next(this.jelovnik);
  }

}
