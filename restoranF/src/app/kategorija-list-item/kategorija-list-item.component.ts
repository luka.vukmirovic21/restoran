import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Kategorija} from "../model/kategorija.model";

@Component({
  selector: 'tr[app-kategorija-list-item]',
  templateUrl: './kategorija-list-item.component.html',
  styleUrls: ['./kategorija-list-item.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class KategorijaListItemComponent implements OnInit {

  @Input()
  kategorija: Kategorija;

  constructor() { }

  ngOnInit() {
  }

}
