import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {KategorijaInterface} from "../model/kategorija.model";

@Component({
  selector: 'app-kategorija-list',
  templateUrl: './kategorija-list.component.html',
  styleUrls: ['./kategorija-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class KategorijaListComponent implements OnInit {

  @Input()
  kategorije: KategorijaInterface[];

  constructor() { }

  ngOnInit() {
  }

}
